package pk.labs.LabA;
import pk.labs.LabA.Contracts.ControlPanel;
import pk.labs.LabA.Contracts.Display;
import pk.labs.LabA.Contracts.Glowny;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "Display2.class";
    public static String controlPanelImplClassName = "controlPanel.class";

    public static String mainComponentSpecClassName = "Glowny.class";
    public static String mainComponentImplClassName = "main.class";
    // endregion

    // region P2
    public static String mainComponentBeanName = null;
    public static String mainFrameBeanName = null;
    // endregion

    // region P3
    public static String sillyFrameBeanName = null;
    // endregion
}
